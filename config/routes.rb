Rails.application.routes.draw do
  require 'sidekiq/web'

  mount Sidekiq::Web => '/sidekiq'

  mount MessageBroker::Api => 'message_broker_api'
end
