# message_broker
Микросервис отправки входящих сообщений

## Гемы
1. `Sidekiq` для менеджента очередей расписания, отправки, дожатий
2. `Grape` для Json API
3. `RSpec` для тестов

## Файлы
- `app/api` - api приложения
- `app/job` - задачи sidekiq
- `lib/classes` - заглушки клиентов мессенджеров + класс для проверки повторной отправки
- `config/yetting.yml` - настройка параметров
- `log/<environment>.log` - Сюда заглушки мессенджеров будут логировать сообщения

## Установка
1. `gem install bundler`
2. `bundle install`

## Запуск
- Набрать в терминале `rails s`
- Набрать в терминале `sidekiq`

## Тестирование
- Набрать в терминале `rspec`
- Либо `curl -X POST -H'Content-Type: application/json' -d'{"message":"Тестовое сообщение", "recipients":{"telegram":["11111111","22222222"]}, "shedule_time":"2019-03-10 00:00:00"}' http://localhost:3000/message_broker_api/send_message`. Тогда в http://localhost:3000/sidekiq можно посмотреть запланированные задачи.

## API
- Endpoint: /message_broker_api/send_message
- Format: json
- Params:

  1. message - сообщение
  2. recipients - hash мессенджер (viber, telegram, whats_app) => список клиентов ([]String)
  3. shedule_time - время отправки сообщения

- Example:

  `{
    "message":"Тестовое сообщение",
    "recipients": {
      "telegram": [
        "11111111",
        "22222222"
      ]
    },
    "shedule_time":"2019-03-10 00:00:00"
  }`

