class DuplicateMessageProtector

  def initialize(job_name, messsage_hash)
    @job_name      = job_name
    @messsage_hash = messsage_hash
  end

  def not_sent(ids)
    not_sent_ids = []

    ids.each do |id|
      unless Redis.current.get("%s_%s_%s" % [@job_name, @messsage_hash, id])
        not_sent_ids << id
      end
    end

    return not_sent_ids
  end

  def remember_ids(ids)
    timeout = Yetting.try(:send_cooldown_minutes) || 30

    ids.each do |id|
      Redis.current.set("%s_%s_%s" % [@job_name, @messsage_hash, id], true, ex: timeout.minutes)
    end
  end
end