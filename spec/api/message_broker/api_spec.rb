require 'rails_helper'

describe MessageBroker::Api do
  include Rack::Test::Methods

  before :each do
    Redis.current.flushall
  end

  def app
    MessageBroker::Api
  end

  context 'POST /message_broker_api/send_message' do
    it 'returns 204' do

      request_data = {
        "message":"Тестовое сообщение",
        "recipients":{
          "telegram":[
            "11111111",
            "22222222"
          ]
        },
        "shedule_time":"2019-03-01 00:00:00 +0300"
      }

      post 'send_message', request_data.to_json, 'CONTENT_TYPE' => 'application/json'
      expect(last_response.status).to eq(204)

      expect(DistributorJob).to have_enqueued_sidekiq_job(request_data)
      DistributorJob.perform_one

      checker = DuplicateMessageProtector.new(
        "TelegramJob",
        Digest::SHA256.hexdigest(request_data[:message])
      )

      expect(checker.not_sent(request_data[:recipients][:telegram])).to match_array(request_data[:recipients][:telegram])

      expect(TelegramJob).to have_enqueued_sidekiq_job(request_data[:message], request_data[:recipients][:telegram])
      TelegramJob.perform_one

      expect(checker.not_sent(request_data[:recipients][:telegram])).to match_array([])
    end
  end
end