class SenderJob
  include Sidekiq::Worker

  sidekiq_retry_in do |count|
    Yetting.try(:retry_timeout) || 300
  end

  # Общая логика отправки сооющений
  def perform(message, ids, retry_count = 0)

    return if retry_count > Yetting.try(:retry_counts).to_i

    # На основе мессенджера и хэша сообщения, будем определять, отправлялось ли конкретному id сообщение, или нет
    checker = DuplicateMessageProtector.new(self.class.to_s, Digest::SHA256.hexdigest(message))

    not_sent_ids = checker.not_sent(ids)

    return if not_sent_ids.blank?

    fail_to_send_ids = client.send_message(message, not_sent_ids)

    sent_ids = not_sent_ids - fail_to_send_ids

    checker.remember_ids(sent_ids)

    # Дожим неотправленных
    self.class.perform_in(Yetting.try(:retry_timeout) || 300, message, fail_to_send_ids, retry_count+1)
  end

  def client
    raise "Don't use this class directly. Use inheritors instead"
  end
end