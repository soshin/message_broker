class WhatsAppJob < SenderJob
  sidekiq_options :queue => :whats_app, :backtrace => 5, :retry => Yetting.try(:retry_counts) || false

  def client
    @client ||= WhatsAppClient.new

    return @client
  end
end