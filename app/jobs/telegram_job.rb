class TelegramJob < SenderJob
  sidekiq_options :queue => :telegram, :backtrace => 5, :retry => Yetting.try(:retry_counts) || false

  def client
    @client ||= TelegramClient.new

    return @client
  end
end