class ViberJob < SenderJob
  sidekiq_options :queue => :viber, :backtrace => 5, :retry => Yetting.try(:retry_counts) || false

  def client
    @client ||= ViberClient.new

    return @client
  end
end