class DistributorJob
  include Sidekiq::Worker

  sidekiq_options :queue => :default, :backtrace => 5, :retry => Yetting.try(:retry_counts) || false

  sidekiq_retry_in do |count|
    Yetting.try(:retry_timeout) || 300
  end

  def perform(incoming_params)
    performing_time = incoming_params["shedule_time"].present? ? Time.parse(incoming_params["shedule_time"]) : Time.now

    incoming_params["recipients"].each do |messenger, ids|

      job_class = case messenger.to_sym
      when :whats_app
        "WhatsAppJob"
      when :telegram
        "TelegramJob"
      when :viber
        "ViberJob"
      else
        next
      end

      job_class.constantize.perform_at(performing_time, incoming_params["message"], ids.uniq)
    end
  end
end