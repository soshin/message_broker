module MessageBroker
  class Api < Grape::API
    format :json

    desc "Запрос на добавление в очередь отправки"
    params do
      requires :message, type: String, desc: 'Текст запроса'
      requires :recipients, type: Hash, desc: 'Список мессенджеров и их клиентов' do
        optional :whats_app, type: Array[String], desc: 'Массив id клиентов WhatsApp'
        optional :viber, type: Array[String], desc: 'Массив id клиентов Viber'
        optional :telegram, type: Array[String], desc: 'Массив id клиентов Telegram'
        at_least_one_of :whats_app, :viber, :telegram
      end
      optional :shedule_time, type: Time
    end
    post :send_message do

      # Раскидывает по очередям мессенджеров
      DistributorJob.perform_async(params)

      status 204
    end
  end
end